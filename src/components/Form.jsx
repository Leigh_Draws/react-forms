import { useState } from "react";

function Form() {

    // Déclaration du state avec la valeur assignée à leur id (dans form) afin d'enregistrer plusieurs state
    // "name" et "age" font référence à l'id de leur input
    const [value, setValue] = useState({ name: "", firstname: "", age: "", email: "", password: "" });

    const [errorMessage, setErrorMessage] = useState(null);

    function validate(inputValue) {
        if (!inputValue) {
            setErrorMessage(`Le champ ne peut pas être vide`);
        } else {
            setErrorMessage(null);
        }
    }
    // Fonction handleChange qui écoute l'évenement quand l'utilisateur écrit
    const handleChange = (event) => {

        // Récupère pour chaque id, la valeur que l'utilisateur écrit dans l'input (on pourrait faire la même chose avec la propriété name dans l'input "name, value")
        const { id, value } = event.target;

        // Avec le setState on enregistre chaque valeur avec son id et sa valeur
        // Puis on étale les valeur avec spread pour les enregistrer dans previousValue
        setValue((previousValue) => ({ ...previousValue, [id]: value }))
        // C'est pas encore très clair

        validate(value);
    }

    // Fonction qui écoute l'évent submit et empêche le rechargement de la page
    // Et affiche dans la console les valeurs de chaque input en les ciblant par leur id
    const handleSubmit = (event) => {
        event.preventDefault();
        console.log(`Le texte saisi est : ${value.name} ${value.firstname} ${value.age} ${value.email} ${value.password}`);

    };

    return (
        <form onSubmit={handleSubmit}>
            <label htmlFor="name">Nom : </label>

            {/* Pour différencier chaque input on a besoin d'un id ou name */}
            {/* On affiche le state "value" qui a enregistré une valeur différente pour chaque input grâce à l'id*/}
            <input
                type="text"
                id="name"
                value={value.name}
                required="required"
                onChange={handleChange}
            />
            <br />
            <label htmlFor="firstname">Prénom : </label>
            <input
                type="text"
                id="firstname"
                value={value.firstname}
                required="required"
                onChange={handleChange}
            />
            <br />
            <label htmlFor="age">Age : </label>
            <input
                type="number"
                id="age"
                value={value.age}
                min={18}
                required="required"
                onChange={handleChange}
            />
            <br />
            <label htmlFor="email">Adresse Email : </label>
            <input
                type="email"
                id="email"
                value={value.email}
                required="required"
                onChange={handleChange}
            />
            <br />
            <label htmlFor="password">Mot de Passe : </label>
            <input
                type="password"
                id="password"
                value={value.password}                
                minLength={8}
                required="required"
                onChange={handleChange}
            />
            <br />
            {errorMessage && <div style={{color: "red"}}> {errorMessage} </div>}
            <button type="submit" disabled={errorMessage}>Envoyer</button>
        </form>
    );
}

export default Form